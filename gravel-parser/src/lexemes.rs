//! Individual lexemes from the Gravel format.

use nom::{
    bytes::complete::{take_while, take_while1},
    IResult,
};
use nom_locate::LocatedSpan;

type Span<'a> = LocatedSpan<&'a str>;

use crate::tokens;

pub(crate) fn header_name(s: Span) -> IResult<Span, Span> {
    take_while1(tokens::is_header_name)(s)
}

pub(crate) fn identifier(s: Span) -> IResult<Span, Span> {
    take_while1(tokens::is_identifier)(s)
}

pub(crate) fn indent(s: Span) -> IResult<Span, Span> {
    take_while(tokens::is_indent)(s)
}

pub(crate) fn object_kind(s: Span) -> IResult<Span, Span> {
    // Object kinds accept the same symbols as identifiers.
    identifier(s)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn lex_identifier() {
        let (remaining, parsed_identifier) = identifier(Span::new("abc_xyz_123")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(parsed_identifier.fragment(), &"abc_xyz_123");

        let (remaining, parsed_identifier) = identifier(Span::new("abc xyz")).unwrap();
        assert_eq!(remaining.fragment(), &" xyz");
        assert_eq!(parsed_identifier.fragment(), &"abc");

        assert!(identifier(Span::new("-xyz")).is_err());
    }

    #[test]
    fn lex_header_name() {
        let (remaining, name) = header_name(Span::new("header-name")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(name.fragment(), &"header-name");

        let (remaining, name) = header_name(Span::new("header-name extra")).unwrap();
        assert_eq!(remaining.fragment(), &" extra");
        assert_eq!(name.fragment(), &"header-name");

        assert!(header_name(Span::new("_header_name")).is_err());
    }
}
