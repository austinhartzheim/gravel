//! Names for specific token sets.

pub(crate) fn is_header_name(c: char) -> bool {
    c.is_lowercase() || c.is_numeric() || c == '-'
}

pub(crate) fn is_identifier(c: char) -> bool {
    c.is_lowercase() || c.is_numeric() || c == '_'
}

pub(crate) fn is_indent(c: char) -> bool {
    let c = c as char;
    c == ' ' || c == '\t'
}

pub(crate) fn is_whitespace(c: char) -> bool {
    c.is_whitespace()
}
