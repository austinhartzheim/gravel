//! Parser for the Gravel file format.
//!
//! Gravel files consist of one or more "objects," where the object has a particular kind,
//! an optional identifier, zero or more headers, and freeform body text.
//!
//! A sample object:
//! ```markdown
//! kind identifier {
//!     :header1: value1
//!     :header2: value2
//!     Freeform text
//! }
//! ```

mod lexemes;
mod parser;
mod tokens;

pub use parser::{document, Object};
