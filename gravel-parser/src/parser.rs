//! Parsers for high-level objects in the Gravel format.

use nom::combinator::opt;
use nom::multi::many0;
use nom::sequence::preceded;
use nom::{
    bytes::complete::{tag, take_until, take_while, take_while1},
    IResult,
};
use nom_locate::LocatedSpan;

type Span<'a> = LocatedSpan<&'a str>;

use crate::lexemes;
use crate::tokens;

type Headers<'a> = Vec<(&'a str, &'a str)>;

/// A Gravel object consists of a kind, optional identifier, zero or more key-value pairs called
/// headers, and a free-text body.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Object<'a> {
    kind: &'a str,
    id: Option<&'a str>,
    headers: Headers<'a>,
    body: &'a str,
}

impl<'a> Object<'a> {
    /// Render the object in a format that the parser will accept.
    pub fn render(&self) -> String {
        let mut s = String::new();
        self.render_to_str(&mut s);
        s
    }

    /// Render the object in a format that the parser will accept.
    pub fn render_to_str(&self, s: &mut String) {
        // Render object declaration
        s.push_str(self.kind);
        s.push(' ');
        if let Some(id) = self.id {
            s.push_str(id);
            s.push(' ');
        }
        s.push_str("{\n");

        // Render headers
        for (k, v) in &self.headers {
            s.push_str("    :");
            s.push_str(k);
            s.push_str(": ");
            s.push_str(v);
            s.push('\n');
        }

        // Render body
        s.push_str(self.body);

        // Closing brace and empty line
        s.push_str("\n}\n");
    }
}

/// Parse a Gravel document, containing one or more objects.
pub fn document(s: Span) -> IResult<Span, Vec<Object>> {
    many0(object_with_whitespace)(s)
}

/// Parse a header, ignoring whitespace at the start of the line.
fn indented_header(s: Span) -> IResult<Span, (Span, Span)> {
    preceded(lexemes::indent, header)(s)
}

/// Parse a header of the form `:header-name: header value`.
fn header(s: Span) -> IResult<Span, (Span, Span)> {
    let (s, _) = tag(":")(s)?;
    let (s, name) = lexemes::header_name(s)?;
    let (s, _) = tag(":")(s)?;
    let (s, _) = take_while1(tokens::is_whitespace)(s)?;
    let (s, value) = take_until("\n")(s)?;
    let (s, _) = tag("\n")(s)?;
    Ok((s, (name, value)))
}

/// Parse input as object headers. Return a `HashMap` mapping header names to their values.
///
/// This function ignores whitespace before the header.
fn headers(s: Span) -> IResult<Span, Headers> {
    let (s, headers) = many0(indented_header)(s)?;
    let headers = headers
        .iter()
        .map(|(name_span, value_span)| (*name_span.fragment(), *value_span.fragment()))
        .collect();
    Ok((s, headers))
}

/// Parse an object's declaration, headers, and body.
fn object(s: Span) -> IResult<Span, Object> {
    let (s, (kind, id)) = object_declaration(s)?;
    let (s, _) = take_while(tokens::is_whitespace)(s)?;
    let (s, (headers, body)) = object_body(s)?;

    Ok((
        s,
        Object {
            kind: kind.fragment(),
            id: id.map(|id| *id.fragment()),
            body: body.fragment(),
            headers,
        },
    ))
}

/// Consume whitespace preceding an object. Return the object.
fn object_with_whitespace(s: Span) -> IResult<Span, Object> {
    // Consume whitespace before parsing the object.
    let (s, _) = opt(take_while(tokens::is_whitespace))(s)?;
    object(s)
}

/// Object bodies consist of two parts. Indented headers and indented freeform text.
///
/// Parse the object body returning the headers and the untouched text.
fn object_body(s: Span) -> IResult<Span, (Headers, Span)> {
    let (s, _) = tag("{")(s)?;
    // If the body starts with a linebreak, don't count that break as a part of the body.
    let (s, _) = opt(tag("\n"))(s)?;
    let (s, headers) = headers(s)?;
    let (s, body) = take_until("\n}")(s)?;
    let (s, _) = tag("\n}")(s)?;
    Ok((s, (headers, body)))
}

/// Object declarations consist of a kind and an optional identifier.
///
/// Example:
/// ```gravel
/// kind identifier {
/// ```
fn object_declaration(s: Span) -> IResult<Span, (Span, Option<Span>)> {
    let (s, kind) = lexemes::object_kind(s)?;
    let (s, _) = take_while1(tokens::is_whitespace)(s)?;
    let (s, id) = opt(lexemes::identifier)(s)?;
    Ok((s, (kind, id)))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn object_render() {
        let object = Object {
            kind: "task",
            id: Some("identifier"),
            headers: Headers::new(),
            body: "    object",
        };
        assert_eq!(object.render(), "task identifier {\n    object\n}\n")
    }

    /// Check that the render method and parser will produce identical results.
    ///
    /// Note: it is acceptable for some minor formatting differences to be normalized, but those
    /// differences are not covered in this test.
    #[test]
    fn object_render_round_trip() {
        let input = r"task identifier {
    :header1: value1
    :header2: value2
    object
}
";
        let (remaining, object) = object(Span::new(input)).unwrap();
        assert_eq!(remaining.fragment(), &"\n");
        assert_eq!(object.render().as_str(), input);
    }

    #[test]
    fn parse_document() {
        let (_remaining, objects) =
            document(Span::new(include_str!("data/simple_document.gravel")))
                .expect("Failed to parse sample document");
        assert_eq!(objects.len(), 2);
        assert_eq!(
            objects[0],
            Object {
                kind: "task",
                id: None,
                headers: Vec::new(),
                body: "    # Task without ID\n    This is a sample task without an identifier."
            }
        );
        assert_eq!(objects[1], Object { kind: "task", id: Some("sample_task"), headers: Headers::new(), body: "    # Task with ID\n    This is a sample task with the identifier `sample_task`."});
    }

    #[test]
    fn parse_header() {
        let (remaining, (name, value)) = header(Span::new(":test: value\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(name.fragment(), &"test");
        assert_eq!(value.fragment(), &"value");

        let (remaining, (name, value)) = header(Span::new(":test:  \t value\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(name.fragment(), &"test");
        assert_eq!(value.fragment(), &"value");

        let (remaining, (name, value)) = header(Span::new(":test: multiple words\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(name.fragment(), &"test");
        assert_eq!(value.fragment(), &"multiple words");

        // No spaces allowed in header names
        assert!(header(Span::new(":test header: multiple words\n")).is_err());
        // Headers must start with ":"
        assert!(header(Span::new("test: multiple words\n")).is_err());
    }

    #[test]
    fn parse_headers() {
        let mut expected = Headers::new();
        expected.push(("a", "b"));
        expected.push(("c", "d"));

        let (remaining, parsed_headers) = headers(Span::new(":a: b\n:c: d\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(parsed_headers, expected);

        let (remaining, parsed_headers) = headers(Span::new(":a: b\n:c: d\nnon-header")).unwrap();
        assert_eq!(remaining.fragment(), &"non-header");
        assert_eq!(parsed_headers, expected.clone());
    }

    #[test]
    fn parse_indented_header() {
        // Parse header with preceding whitespace
        let (remaining, (key, value)) = indented_header(Span::new("    :test: value\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(key.fragment(), &"test");
        assert_eq!(value.fragment(), &"value");

        let (remaining, (key, value)) = indented_header(Span::new("\t:test:  \t value\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(key.fragment(), &"test");
        assert_eq!(value.fragment(), &"value");

        let (remaining, (key, value)) =
            indented_header(Span::new("    :test: multiple words\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(key.fragment(), &"test");
        assert_eq!(value.fragment(), &"multiple words");

        // Preceding whitespace is optional
        let (remaining, (key, value)) = indented_header(Span::new(":test: value\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(key.fragment(), &"test");
        assert_eq!(value.fragment(), &"value");

        let (remaining, (key, value)) = indented_header(Span::new(":test:  \t value\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(key.fragment(), &"test");
        assert_eq!(value.fragment(), &"value");

        let (remaining, (key, value)) =
            indented_header(Span::new(":test: multiple words\n")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(key.fragment(), &"test");
        assert_eq!(value.fragment(), &"multiple words");

        // No spaces allowed in header names
        assert!(indented_header(Span::new(":test header: multiple words\n")).is_err());
        // Headers must start with ":"
        assert!(indented_header(Span::new("test: multiple words\n")).is_err());
        assert!(indented_header(Span::new("\ttest: multiple words\n")).is_err());
        assert!(indented_header(Span::new("    test: multiple words\n")).is_err());
    }

    #[test]
    fn parse_object() {
        let (remaining, object) =
            object_with_whitespace(Span::new("task id {\n    # title\n}")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(
            object,
            Object {
                kind: "task",
                id: Some("id"),
                headers: Headers::new(),
                body: "    # title"
            }
        )
    }

    #[test]
    fn parse_object_with_whitespace() {
        let (remaining, object) =
            object_with_whitespace(Span::new("task id {\n    # title\n}")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(
            object,
            Object {
                kind: "task",
                id: Some("id"),
                headers: Headers::new(),
                body: "    # title"
            }
        );
    }

    #[test]
    fn parse_object_body() {
        let (remaining, (headers, body)) =
            object_body(Span::new("{    # heading\n    text\n\n    more\n}")).unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(headers, Headers::new());
        assert_eq!(body.fragment(), &"    # heading\n    text\n\n    more");

        let mut expected_headers = Headers::new();
        expected_headers.push(("test", "value"));
        let (remaining, (headers, body)) = object_body(Span::new(
            "{    :test: value\n    # heading\n    text\n\n    more\n}",
        ))
        .unwrap();
        assert_eq!(remaining.fragment(), &"");
        assert_eq!(headers, expected_headers);
        assert_eq!(body.fragment(), &"    # heading\n    text\n\n    more");
    }

    #[test]
    fn parse_object_declaration_position() {
        let (_remaining, (kind, id)) = object_declaration(Span::new("task id {")).unwrap();
        assert_eq!(kind.location_line(), 1);
        assert_eq!(kind.get_utf8_column(), 1);

        let id = id.unwrap();
        assert_eq!(id.location_line(), 1);
        assert_eq!(id.get_column(), 6);
    }

    #[test]
    fn parse_object_body_position() {
        let (_remaining, (_headers, body)) =
            object_body(Span::new("{\n    # heading\n    text\n\n    more\n}")).unwrap();
        assert_eq!(body.location_line(), 2);
        assert_eq!(body.get_utf8_column(), 1);
    }
}
